% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%

\makeatletter
\let\@twosidetrue\@twosidefalse
\let\@mparswitchtrue\@mparswitchfalse
\makeatother

\documentclass{../llncs}
%
\usepackage{makeidx}  % allows for indexgeneration
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[left=3cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}

\title{Boolean Functions for Cryptography}
\author{Rusydi H. Makarim}
\institute{}
%
\begin{document}
\maketitle

\section{Introduction}
The finite field with two elements is denoted by $\mathbb{F}_2 = \{ 0, 1\}$. We define $\mathbb{F}_2^n$ as an $n$-dimensional vector space over $\mathbb{F}_2$.
$$
	\mathbb{F}_2^n = \{ (x_1, \ldots, x_n) \mid x_i \in \mathbb{F}_2,\ \forall i = 1, \ldots, n\}
$$
Elements of $\mathbb{F}_2^n$ is written using an overline $\overline{x} = (x_1, \ldots, x_n)$. The addition in $\mathbb{F}_2$ and $\mathbb{F}_2^n$ is denoted by $+$ and $\oplus$, respectively.

The set $\text{Supp}(\overline{x}) = \{ i \mid x_i \neq 0\}$ is called the support of vector $\overline{x}$. The weight of $\overline{x}$ is defined as $\text{wt}(\overline{x}) = |\text{Supp}(\overline{x})|$. For a vector $\overline{u} \in \mathbb{F}_2^n$, we say that $\overline{x}$ is covered by $\overline{u}$ (or $\overline{u}$ covers $\overline{x}$) if $x_i \leq u_i$ for all $i = 1, \ldots, n$. We denote it using $\overline{x} \preceq \overline{u}$ to indicate that $\overline{x}$ is covered by $\overline{u}$. The dot product of two vectors $\overline{x}$ and $\overline{y}$ is defined as $\overline{x} \cdot \overline{y} = \sum_{i=1}^{n} x_i y_i$.
\begin{theorem}
	Let $\overline{u} \in \mathbb{F}_2^n$. The set $\mathcal{V}_{\overline{u}} = \{ \overline{x} \in \mathbb{F}_2^n \mid \overline{x} \preceq \overline{u} \}$ forms a vector subspace of $\mathbb{F}_2^n$.
\end{theorem}
\begin{proof}
We have $\overline{0} \in \mathcal{V}_{\overline{u}}$ since $\overline{0} \preceq \overline{u}$ for every $\overline{u} \in \mathbb{F}_2^n$. The closedness of scalar multiplication is trivial to show. In order to show that addition is closed, let $\overline{x}, \overline{y} \in \mathcal{V}_{\overline{u}}$. If $u_i = 0$ then $x_i = 0, y_i = 0$ which implies that $x_i + y_i \leq u_i$. If $u_i = 1$ then definitely $x_i + y_i \leq u_i$. Clearly $\overline{x} \oplus \overline{y} \preceq \overline{u}$ and therefore $\overline{x} \oplus \overline{y} \in V_{\overline{u}}$. \qed
\end{proof}
\begin{remark}
The cardinality of $\mathcal{V}_{\overline{u}}$ is equal to $2^{\text{wt}(\overline{u})}$ and $\dim(\mathcal{V}_{\overline{u}}) = \text{wt}(\overline{u})$. A trivial basis of $\mathcal{V}_{\overline{u}}$ is the set $\{ e_i \mid i \in \text{Supp}(\overline{u}) \}$ where $e_i$ is the $i$-th canonical basis vector of $\mathbb{F}_2^n$.
\end{remark}

\begin{theorem}
Let $\overline{x}, \overline{u} \in \mathbb{F}_2^n$. The vector $\overline{x}$ is covered by $\overline{u}$ if and only if $\text{Supp}(\overline{x}) \subseteq \text{Supp}(\overline{u})$.
\end{theorem}
\begin{proof}
Suppose $\overline{x} \preceq \overline{u}$ and let $i \in \text{Supp}(\overline{x})$. Clearly, $x_i = 1$ and it follows that $u_i = 1$. Hence $i \in \text{Supp}(\overline{u})$ and therefore $\text{Supp}(\overline{x}) \subseteq \text{Supp}(\overline{u})$.\\
Now suppose that $\text{Supp}(\overline{x}) \subseteq \text{Supp}(\overline{u})$ and let $i \in \{ 1, \ldots, n\}$. We have the following situations
\begin{enumerate}
	\item If $i \in \text{Supp}(\overline{x})$ then $i \in \text{Supp}(\overline{u})$. It implies that $x_i = 1 \leq u_i = 1$.
	\item If $i \not\in \text{Supp}(\overline{x})$ then the following two conditions are possible
	\begin{enumerate}
		\item If $i \in \text{Supp}(\overline{u})$ we have $x_i = 0 \leq u_i = 1$.
		\item If $i \not\in \text{Supp}(\overline{u})$ we have $x_i = 0 \leq u_i = 0$.
	\end{enumerate}
\end{enumerate}
All above conditions shows that $x_i \leq u_i$ for all $i \in \{1, \ldots, n\}$ and therefore $\overline{x} \preceq \overline{u}$. \qed
\end{proof}

\section{Boolean Functions}
\begin{definition}
	An $n$-variable Boolean function $f$ is a mapping from $\mathbb{F}_2^n$ to $\mathbb{F}_2$.
\end{definition}

\begin{example}
Let $f : \mathbb{F}_2^2 \mapsto \mathbb{F}_2$ such that $f(0, 0) = 0,\ f(0, 1) = 1,\ f(1, 0) = 1,\ f(1, 1) = 0$. Such function can be represented in a table below
	\begin{table}
		\centering
		\caption{Truth table of $f$}
		\begin{tabular}{|c|c|}
			\hline
			$\overline{x}$ & $f(\overline{x})$ \\
			\hline
			$(0, 0)$ & $0$\\
			$(0, 1)$ & $1$\\
			$(1, 0)$ & $1$\\
			$(1, 1)$ & $0$\\
			\hline
		\end{tabular}
	\end{table}
\end{example}

The set of all $n$-variable Boolean functions is denoted by $\mathcal{BF}_n$. It is trivial to show that $|\mathcal{BF}_n| = 2^{2^n}$.

\begin{definition}[Pseudo-Boolean function] For a Boolean function $f : \mathbb{F}_2^n \mapsto \mathbb{F}_2$, we define its corresponding pseudo-Boolean function $\mathfrak{f} : \mathbb{F}_2^n \mapsto \mathbb{R}$ (or the codomain can also be considered as $\mathbb{Z}$, instead of $\mathbb{R}$).
\end{definition}

\begin{definition}[Sign function] The sign function of Boolean function $f$ is defined as $$\widehat{f}(\overline{x}) = -1^{\mathfrak{f}(\overline{x})} = 1 - 2 \mathfrak{f}(\overline{x})$$
\end{definition}

\begin{definition}[Support]
	The support of an $n$-variable Boolean function $f$ is defined as $\text{Supp}(f) = \{ \overline{x} \in \mathbb{F}_2^n \mid f(\overline{x}) \neq 0\}$.
\end{definition}

\begin{definition}[Weight]
	The weight (or Hamming weight) of an $n$-variable Boolean function $f$ is defined as $\text{wt}(f) = |\text{Supp}(f)|$. Equivalently, the weight of a Boolean function is the number of $1$'s in its truth table.
\end{definition}

\begin{definition}[Distance]
	Let $f, g$ be $n$-variable Boolean functions. The distance (or Hamming distance) of $f$ and $g$ is defined as
	$$
		\text{dt}(f, g) = | \{ \overline{x} \in \mathbb{F}_2^n \mid f(\overline{x}) \neq g(\overline{x})\} |
	$$
	Equivalently $\text{dt}(f, g) = \text{wt}(f \oplus g)$.
\end{definition}

\begin{definition}[Balanced function] An $n$-variables Boolean function $f$ is balanced iff $\text{wt}(f) = 2^{n-1}$, that is the number of $1$'s and $0$'s in its truth table are equal.
\end{definition}

\begin{definition}[Algebraic Normal Form (ANF)]
The representation of a Boolean function $f$ as a multivariate polynomial in terms of $x_1, \ldots, x_n$
$$
	f(\overline{x}) = \sum_{\overline{u} \in \mathbb{F}_2^n} a_{\overline{u}} \left( \prod_{i=1}^{n} x_j^{u_i} \right)
$$
is called the algebraic normal form (ANF) of $f$.
\end{definition}

\begin{remark}
	The ANF of a Boolean function $f$ can be seen as an element of $\mathbb{F}_2[x_1, \ldots, x_n]/\langle x_1, \ldots, x_n \rangle$.
\end{remark}

\begin{theorem}
Let $f$ be an $n$-variable Boolean function and let $\sum_{\overline{u} \in \mathbb{F}_2^n} a_{\overline{u}} \left( \prod_{i = 1}^{n} x_i^{u_i}\right)$ be its ANF. We have
$$
	a_{\overline{u}} = \sum_{\overline{x} \preceq \overline{u}} f(\overline{x})
$$
\end{theorem}
\begin{proof}
TODO.\qed
\end{proof}

\begin{definition}[Degree]
The degree\footnote{Several authors called it nonlinear order.} of a Boolean function $f(\overline{x}) = \sum_{\overline{u} \in \mathbb{F}_2^n} a_{\overline{u}} \left( \prod_{i=1}^{n} x_j^{u_i} \right)$ is defined as
$$
	\deg(f) = \max \{ \text{wt}(\overline{u}) \mid a_{\overline{u}} \neq 0\}
$$
\end{definition}

\begin{definition}[Affine function]
A Boolean function in the form $a_1 x_1 + \cdots + a_n x_n + c$ is called affine function. It can be written as $\overline{a} \cdot \overline{x} + c$ where $\overline{a} = (a_1, \ldots, a_n)$.
\end{definition}

\begin{definition}[Linear function]
An affine Boolean function $\overline{a} \cdot \overline{x} + c$ with $c = 0$ is called linear function.
\end{definition}

\begin{remark}
	The set of all $n$-variable affine functions is denoted by $\mathcal{A}_n$. Similarly, the set of all $n$-variable linear functions is denoted by $\mathcal{L}_n$. Note that $\mathcal{L}_n \subseteq \mathcal{A}_n$ with $|\mathcal{A}_n| = 2^{n+1}$ and $|\mathcal{L}_n| = 2^n$.
\end{remark}

\begin{remark}
\label{rmk:linear_func_hom}
Let $\overline{\omega} \cdot \overline{x}$ be a linear function and let $\overline{u}, \overline{v} \in \mathbb{F}_2^n$. We have the following
\begin{align*}
	\overline{\omega} \cdot (\overline{u} \oplus \overline{v}) &= \overline{\omega} \cdot (u_1 + v_1, \ldots u_n + v_n)\\
	&= \omega_1(u_1 + v_1) + \cdots + \omega_n(u_n + v_n)\\
	& = \omega_1 u_1 + \omega_1 v_1 + \cdots + \omega_n u_n + \omega_n v_n\\
	&= \overline{\omega} \cdot \overline{u}+ \overline{\omega} \cdot \overline{v}
\end{align*}
Thus, a linear function is essentially a group homomorphism from $\mathbb{F}_2^n$ to $\mathbb{F}_2$.
\end{remark}

\begin{theorem}
Every non-constant affine function is balanced.
\end{theorem}
\begin{proof}
In order to show that all nonconstant affine function is balanced, it is sufficient to prove that all nonconstant linear function is balanced. The result immediately implies that their complement is also balanced, and we may conclude that all nonconstant affine function is balanced.

Let $l_{\overline{\omega}}(\overline{x}) = \overline{\omega} \cdot \overline{x}$ be a nonconstant linear function. Recall from Remark~\ref{rmk:linear_func_hom} that $l_{\overline{\omega}}$ is a group homomorphism from $\mathbb{F}_2^n$ to $\mathbb{F}_2$. Let $\text{Ker}_{l_{\overline{\omega}}}, \text{Im}(l_{\overline{\omega}})$ denote the kernel and image of $l_{\overline{\omega}}$, respectively.

The first isomorphism theorem states that the quotient group $\mathbb{F}_2^n/\text{Ker}_{l_{\overline{\omega}}}$ is isomorphic to $\text{Im}(l_{\overline{\omega}})$. By its homomorphicity $l_{\overline{\omega}}(\overline{0}) = 0$ and since $l_{\overline{\omega}}$ is a nonconstant, then there exists a $\overline{v} \in \mathbb{F}_2^n$ such that $l_{\overline{\omega}}(\overline{v}) = 1$. Hence $l_{\overline{\omega}}$ is surjective (onto) and $|\text{Im}(l_{\overline{\omega}})| = 2$. Since $\mathbb{F}_2^n/\text{Ker}_{l_{\overline{\omega}}} \cong \text{Im}(l_{\overline{\omega}})$, it follows that $|\text{Ker}_{l_{\overline{\omega}}}| = 2^{n-1}$ and clearly $l_{\overline{\omega}}$ is balanced. Since the complement of $l_{\overline{\omega}}$ is also balanced, therefore all nonconstant affine functions are balanced. This completes the proof. \qed
\end{proof}

\begin{definition}[Indicator of subspace] Let $E$ be a subspace of $\mathbb{F}_2^n$. The indicator of $E$ (also called the characteristic function of $E$), is a function $1_E : \mathbb{F}_2^n \mapsto \mathbb{F}_2$ such that
$$
	1_E(\overline{x}) = \left\{
		\begin{array}{ll}
			1 & \mbox{if }\overline{x} \in E\\
			0 & \mbox{if }\overline{x} \not\in E
		\end{array}
	\right.		
$$
\end{definition}

\begin{definition}[Annihilator]
	An annihilator of a Boolean function $f$ is a function $g$ such that $f(\overline{x})  g(\overline{x}) =~0$ for all $\overline{x}$. The set of all annihilator of $f$ is denoted by $\text{An}(f)$.
\end{definition}

\begin{theorem}
	Let $\mathcal{R} = \mathbb{F}_2[x_1, \ldots, x_n] / \langle x_1^2 - x_1, \ldots, x_n^2 - x_n \rangle$ and let $f \in \mathcal{R}$ be an $n$-variable Boolean function. The set $\text{An}(f)$ is a principal ideal in $\mathcal{R}$ generated by $f + 1$, i.e. $\text{An}(f) = \langle f + 1 \rangle = \{ (f+1)r \mid r \in \mathcal{R}\}$. Its cardinality is equal to $|\text{An}(f)| = 2^{2^n - \text{wt}(f)}$. If $f$ is balanced, then $|\text{An}(f)| = 2^{2^{n-1}}$.
\end{theorem}
\begin{proof}
First, we will show that $\text{An}(f)$ is an ideal in $\mathcal{R}$. Obviously the zero polynomial $0$ is in $\text{An}(f)$ because $f \cdot 0 = 0$. To prove that the addition is closed, let $g, h \in \text{An}(f)$. We have $f \cdot (g + h) = f \cdot g + f \cdot h = 0$. Hence $g + h \in \text{An}(f)$. Next, let $r \in \mathcal{R}$. It follows that $f \cdot (r \cdot g) = f \cdot (g \cdot r) = (f \cdot g) \cdot r = 0 \cdot r = 0$. Thus $r \cdot g = g \cdot r \in \text{An}(f)$. This shows that $\text{An}(f)$ is an ideal in $\mathcal{R}$.

In order to show that $\text{An}(f) = \langle f + 1 \rangle$ assume that $h \in \text{An}(f)$ and $h \not\in \langle f + 1 \rangle$. We then have $f \cdot h = 0$, which implies that $(f + 1) \cdot h = f \cdot h + h = h$. Therefore $\text{An}(f) = \langle f + 1 \rangle$.

To prove the cardinality of $\text{An}(f)$, let $g \in \text{An}(f)$. For every $\overline{x} \in \mathbb{F}_2^n$ such that $f(\overline{x}) = 1$, then $g(\overline{x}) = 0$. For $\overline{y} \in \mathbb{F}_2^n$ such that $f(\overline{y}) = 0$, then $g(\overline{y})$ can be set to an arbitrary values in $\mathbb{F}_2$, giving $2^{2^n - \text{wt}(f)}$ possible such $g$. Clearly $|\text{An}(f)| = 2^{2^n - \text{wt}(f)}$. Obviously if $f$ is balanced, then $|\text{An}(f)| = 2^{2^n / 2} = 2^{2^{n-1}}$.\qed
\end{proof}

\begin{corollary}
Let $f : \mathbb{F}_2^n \mapsto \mathbb{F}_2$ be a nonaffine balanced Boolean function. Then $\text{An}(f)$ contains exactly one balanced function, namely $f + 1$. In particular, there are no nonzero affine functions in $\text{An}(f)$.
\end{corollary}
\begin{proof}
In order for $g$ to satisfy $f \cdot g = 0$, then it must be the case that $g(\overline{x}) = 0$ whenever $f(\overline{x}) = 1$. Since $f$ is balanced, it follows that $\text{wt}(g) \leq 2^{n-1}$. If $g$ is to be a balanced function, then it must be that $g = f + 1$.
In particular since any affine functions is balanced and, by assumption, $f + 1$ is nonlinear, thus there are no nonzero affine functions in $\text{An}(f)$. \qed
\end{proof}

\begin{corollary}
There is exactly one nonzero annihilator of degree $1$ for any affine function $a \in \mathcal{A}_n$ given by $a + 1$.
\end{corollary}
\begin{proof}
Since $a$ is affine, hence it is balanced. It implies that the only balanced annihilator is of the form $a + 1$, which is also affine.\qed
\end{proof}

\begin{definition}[Algebraic Immunity] Algebraic immunity of a Boolean function $f$ is the smallest degree of the function $g$ for which $f(\overline{x})g(\overline{x}) = 0$ or $(f(\overline{x}) + 1) g(\overline{x}) = 0$.
\end{definition}

\begin{definition}[Cross Correlation]
The cross-correlation of Boolean functions $f, g$ at $\overline{\alpha}$ is defined as
$$
	C_{f, g}(\overline{\alpha}) = \sum_{\overline{x} \in \mathbb{F}_2^n} \widehat{f}(\overline{x}) \widehat{g}(\overline{x} \oplus \overline{\alpha})
$$
\end{definition}

\begin{definition}[Correlation]
The correlation between Boolean functions $f$ and $g$ is defined as
$$
	C_{f, g} = 	\sum_{\overline{x} \in \mathbb{F}_2^n} \widehat{f}(\overline{x}) \widehat{g}(\overline{x})
$$
Equivalently we can also define it as $\widehat{f} \cdot \widehat{g}$, the inner product of the sign truth table of $f$ and $g$.
\end{definition}

\begin{remark}
Correlation of $f, g$ is a special case of cross-correlation of $f, g$ at $\overline{0}$.
\end{remark}

\begin{theorem}[Correlation and Hamming distance] The correlation of $f$ and $g$ can be expressed in terms of the distance of $f$ and $g$
$$
	C_{f, g} = 2^n - 2 \cdot \text{dt} (f, g)
$$
\end{theorem}
\begin{proof}
For each $\overline{x} \in \mathbb{F}_2^n$ we have the following cases
$$
	\widehat{f}(\overline{x}) \widehat{g}(\overline{x}) = \left\{
		\begin{array}{ll}
			-1 & \mbox{if }\widehat{f}(\overline{x}) \neq \widehat{g}(\overline{x}) \\
			1 & \mbox{if }\widehat{f}(\overline{x}) = \widehat{g}(\overline{x})
		\end{array}
	\right.		
$$
It follows that $|\{ \overline{x} \in \mathbb{F}_2^n \mid \widehat{f}(\overline{x}) \neq \widehat{g}(\overline{x})\}| = \text{dt}(f, g)$ and $|\{ \overline{x} \in \mathbb{F}_2^n \mid \widehat{f}(\overline{x}) = \widehat{g}(\overline{x})\}| =2^n - \text{dt}(f, g)$. Thus, $C_{f, g} = 	\sum_{\overline{x} \in \mathbb{F}_2^n} \widehat{f}(\overline{x}) \widehat{g}(\overline{x}) = 2^n - \text{dt}(f, g) - \text{dt}(f, g) = 2^n - 2 \cdot \text{dt} (f, g)$. \qed
\end{proof}

\begin{definition}[Autocorrelation]
The autocorrelation of a Boolean function $f$ at $\overline{\alpha}$ is defined as
$$
	r_{f}(\overline{\alpha}) = \sum_{\overline{x} \in \mathbb{F}_2^n} \widehat{f}(\overline{x}) \widehat{f}(\overline{x} \oplus \overline{\alpha})
$$
\end{definition}

\begin{remark}
The Boolean function $f(\overline{x} \oplus \overline{\alpha})$ is only a permutation of the truth table of $f$. Autocorrelation can be seen as a correlation of a Boolean function $f$ with its own permutation using $\overline{\alpha}$ as the parameter.
\end{remark}

\begin{definition}[Fourier Transform]
\end{definition}

\begin{definition}[Walsh Transform]
The correlation of a Boolean function $f$ with a linear function $\overline{\omega} \cdot \overline{x}$ is called Walsh transform of $f$ at $\overline{\omega}$
$$
	\mathcal{W}_f(\overline{\omega}) = \sum_{\overline{x} \in \mathbb{F}_2^n} \widehat{f}(\overline{x}) (-1)^{\overline{\omega} \cdot \overline{x}}
$$
\end{definition}

\begin{definition}[Hadamard Matrix]
\end{definition}

\begin{theorem}[Parseval's Theorem]
\end{theorem}

\begin{theorem}[Wiener-Khintchine Theorem]
\end{theorem}

\begin{definition}[Symmetric functions]
\end{definition}

\begin{definition}[Bent functions]
\end{definition}

\begin{definition}[Derivative] Let $f$ be an $n$-variables Boolean function. The derivative of $f$ at $\overline{\alpha} \in \mathbb{F}_2^n$ is defined as $D_{\overline{\alpha}}f(\overline{x}) = f(\overline{x} \oplus \overline{\alpha}) + f(\overline{x})$.
\end{definition}

\begin{definition}[Linear structures] Let $f$ be an $n$-variables Boolean function. If $D_{\overline{\alpha}}f(\overline{x}) = c$ for all $\overline{x} \in \mathbb{F}_2^n$, that is $D_{\overline{\alpha}}f$ is a constant function, then we say that $\overline{\alpha}$ is a $c$-linear structure of $f$.
\end{definition}

\begin{remark}
The zero vector $\overline{0}$ is a trivial linear structure of $f$.
\end{remark}

\begin{definition}[Nonlinearity]
The nonlinearity of an $n$-variable Boolean function $f$ is its minimum distance to the set of all affine functions,
$$
	\mathcal{NL}_f = \min_{g \in \mathcal{A}_n} \text{dt}(f, g)
$$
\end{definition}

\begin{definition}[Correlation Immunity]
\end{definition}

\begin{definition}[Resiliency]
\end{definition}

\begin{definition}[Strict Avalanche Criterion]
\end{definition}

\begin{definition}[Propagation Criterion]
\end{definition}

\begin{definition}[Nonexistence of Linear Structures]
\end{definition}

\begin{definition}[Algebraic Immunity]
\end{definition}

\section{Substitution Boxes (S-Boxes)}

\begin{definition}[S-Boxes] Let $n, m \in \mathbb{Z}_{>0}$ be positive integers. An $n \times m$ S-Box $S$ is defined as the mapping $S : \mathbb{F}_2^n \mapsto \mathbb{F}_2^m$.
\end{definition}

\begin{definition}[Coordinate functions]
For $\overline{x} \in \mathbb{F}_2^n$ and $\overline{y} = (y_1, \ldots, y_m) \in \mathbb{F}_2^m$ with $S(\overline{x}) = \overline{y}$, the Boolean functions $f_i$ such that $y_i = f_i(\overline{x})$ are called the coordinate functions of $S$.
\end{definition}

\begin{definition}[Component functions]
For every nonzero $\overline{b} \in \mathbb{F}_2^m$, the functions $\overline{b} \cdot S(\overline{x})$ are called the component functions of $S$. Note that $\overline{b} \cdot S(\overline{x}) = \sum_{i=1}^{m} b_i f_i(\overline{x})$
\end{definition}

\begin{definition}[Balanced S-Boxes]
An $n \times m$ S-Box is balanced if it takes every value of $\mathbb{F}_2^m$ the same number $2^{n-m}$ times.
\end{definition}

\begin{remark}
	Every $n \times n$ bijective S-Box $S$ (permutation) is balanced.
\end{remark}

\begin{definition}[Degree]
The degree of an $n \times m$ S-Box $S$, $\deg(S)$, is defined as the highest degree of algebraic normal form of its coordinate functions.
\end{definition}
\begin{remark}
The notion of degree in this case is defined as a notion related to algebraic attack. One may also define the degree of an S-Box as a minimal algebraic degree of its component functions. This may related to different type of attack such as linear cryptanalysis.
\end{remark}

\begin{theorem}[Univariate Representation]
\label{thm:univariate_rep}
Let $S : \mathbb{F}_2^n \mapsto \mathbb{F}_2^n$ be a permutation. Then there exists a unique univariate polynomial $F$ in $\mathbb{F}_{2^n}[X] / \langle X^{2^n} + X \rangle $ such that
$$
	F(X) = \sum_{i=0}^{2^n - 1} A_i X^i \qquad A_i \in \mathbb{F}_{2^n}.
$$
Moreover $A_0 = \varphi (S(0))$ and $A_{2^n - 1} = \sum_{x \in \mathbb{F}_2^n} \varphi(S(x))$ and the remaining coefficients $A_i,\ 1 \leq 2^n - 2$ are given by the discrete Fourier transform of the values of $S$ at all nonzero inputs, namely
$$
	A_i = \sum_{k=0}^{2^n - 2} S(\alpha^k) \alpha^{-ki} \quad 1 \leq i \leq 2^n - 2
$$
where $\langle \alpha \rangle = \mathbb{F}_{2^n}^*$ and $\varphi : \mathbb{F}_2^n \mapsto \mathbb{F}_{2^n}$ is the canonical isomorphism.
\end{theorem}

\begin{example}
Let $S$ be a $3 \time 3$ S-Box defined as $S = (0, 1, 5, 6, 7, 2, 3, 4)$. Let $\alpha$ be the root of irreducible polynomial $f = X^3 + X + 1 \in \mathbb{F}_2[X]$. We can construct $\mathbb{F}_8$ using the irreducible polynomial $f$ and since $f$ is a primitive polynomial, then $\alpha$ generates the nonzero element of $\mathbb{F}_8$, i.e, $\mathbb{F}_8 = \{ 0, 1, \alpha, \alpha^2, \ldots, \alpha^{6} \}$. The canonical isomorphism $\varphi$ can be defined as
$$
	\varphi(x_1, x_2, x_3) = x_1 \alpha^2+ x_2 \alpha + x_3.
$$
We define the mapping $S' = \varphi \circ S(x_1, x_2, x_3) \in \mathbb{F}_{8}[X]$ and we would like to get the polynomial representation of $'$.

One can use Lagrange interpolation to obtain univariate polynomial representation of $S'$. Here we demonstrate it using the result from Theorem~\ref{thm:univariate_rep}. The complete mapping of $S$ and $S'$ is given in the following table

\begin{table}[h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
		\hline
	 	$(x_0, x_1, x_2)$ & $(0,0,0)$ & $(0,0,1)$ & $(0,1,0)$ & $(0,1,1)$ & $(1,0,0)$ & $(1,0,1)$ & $(1,1,0)$ & $(1,1,1)$ \\
	 	\hline
	 	$S$ & $(0, 0, 0)$ & $(0, 0, 1)$ & $(1, 0, 1)$ & $(1, 1, 0)$ & $(1, 1, 1)$ & $(0, 1, 0)$ & $(0, 1, 1)$ & $1, 0, 0$\\
	 	\hline
	 	$S'$ & $0$ & $1$ & $\alpha^6$ & $\alpha^4$ & $\alpha^5$ & $\alpha$ & $\alpha^3$ & $\alpha^2$\\
	 	\hline
	\end{tabular}
\end{table}
One can verify that $A_6 = 1$ and $A_i = 0$ for $i \in \{ 0, 1, 2, 3, 4, 5, 7\}$.  Thus the polynomial representation of $S'$ is
$$
	S'(X) = X^6
$$
\end{example}

\begin{definition}[Monomial/Power function]
An $n$-bit permutation $S$ in which its univariate representation contains a single monomial (up to a change of basis) is called \textbf{monomial} or \textbf{power} functions.
\end{definition}

\begin{example}
The S-Box $S$ in the previous example is a power function.
\end{example}

\begin{definition}
Let $S$ be an $n$-bit S-Box and let
$$
	\sum_{i=0}^{2^n - 1} A_i X^i
$$
denote its univariate representation in $\mathbb{F}_{2^n}[X]$. Then
$$
	\deg(S) = \max \{ \text{wt}(i) \mid A_i \neq 0, 0 \leq i \leq 2^n - 1 \}
$$
\end{definition}



\begin{theorem}
\label{thm:powerfuncperm}
Let $S : x \mapsto x^s$ over $\mathbb{F}_{2^n}$. Then $S$ is a permutation if and only if $\gcd(s, 2^n - 1) = 1$.
\end{theorem}

\begin{theorem}
\label{thm:cubefuncperm}
The monomial function $F_3 : \mathbb{F}_{2^n} \mapsto \mathbb{F}_{2^n}$ given by $F_3(x) = x^3$ is a permutation if and only if $n$ is odd.
\end{theorem}
\begin{proof}
In this proof, we use the previous theorem and a well-known fact that $\gcd(n^p - 1, n^q - 1) = n^{\gcd(p, q)} - 1$ for some positive integers $p, q$. We need to characterize $n$ for which $\gcd(3, 2^n-1) = 1$.
\begin{align*}
	1 &= \gcd(3, 2^n - 1) = \gcd(2^2 - 1, 2^n - 1)\\
	   &= 2^{\gcd(2, n)} - 1
\end{align*}
This implies that $\gcd(2, n) = 1$ and thus $n$ must be odd number.\qed
\end{proof}

\begin{example}
When $F_3 : X \mapsto X^3$ is a permutation of $\mathbb{F}_{2^n}$, give expression to its inverse $F_3^{-1}$.
Recall that $n$ is an odd number. The inverse permutation $F_3^{-1}$ is of the form $F_3^{-1}(Y) = Y^{s'}$ such that $F_3^{-1}(X^3) = (X^3)^{s'} = X^{3s'} = X$, that is $3s' \equiv 1 \pmod{2^n - 1}$.
Note also that $2^{n+1} - 1 \equiv 1 \pmod{2^n - 1}$. This implies that the value $s'$ that satisfy the inverse is
$$
	s' = \dfrac{2^{n+1} - 1}{3}.
$$
The numerator $2^{n+1} - 1$ can be seen as a sum of geometric progression
$$
	2^{n+1} - 1 = \dfrac{1 - 2^{n+1}}{1 - 2}
$$
with ratio $r = 2$ and the first element in the sequence equal to $2^0$. We can have the following result
\begin{align*}
	2^{n+1} - 1 = \dfrac{1 - 2^{n+1}}{1 - 2} &= 2^0 + 2^1 + 2^2 + \cdots + 2^{n}\\
	&= \sum_{i=0}^{ \lfloor n/2 \rfloor } 2^{2i} + \sum_{j=0}^{\lfloor n/2 \rfloor} 2^{2j + 1}\\
	&= \sum_{i=0}^{ \lfloor n/2 \rfloor } 2^{2i} + \sum_{j=0}^{\lfloor n/2 \rfloor} 2^{2j} \cdot 2\\
	&= 3 \sum_{i=0}^{ \lfloor n/2 \rfloor} 2^{2i}.
\end{align*}
Using above result and the fact that $n$ is odd, we can now simplify the value $s'$ as follows
$$
	s' = \dfrac{2^{n+1} - 1}{3} = \sum_{i=0}^{(n-1)/2} 2^{2i}.
$$
We can also deduce that $\deg(F_3^{-1}) = \text{wt}(s') = (n+1)/2$.
\end{example}

\begin{example}
Let $n$ be an even integer and $s$ be an integer $1 \leq s \leq 2^n - 1$. The purpose of this example is to show that there exists no APN power permutation on $\mathbb{F}_{2^n}$ when $n$ is even.
\begin{enumerate}
	\item Let $\alpha$ be a primitive element of $\mathbb{F}_{2^n}$ and $\beta = \alpha^{(2^n - 1)/3}$. Prove that $\beta$ is a solution of 
	$$
		(x + 1)^s + x^s = 1
	$$
	unless $s \equiv 0 \pmod{3}$.\\
	Notice that $\beta^3 = \alpha^{2^n - 1} = 1$ and since $\beta$ is a nonzero element, it implies that $\beta \in \mathbb{F}_4^*$. Looking back at the equation $(x + 1)^s + x^s = 1$ and using the fact that $\beta \in \mathbb{F}_4^*$, then there are only $3$ possible values of $s$
	\begin{enumerate}
		\item $s \equiv 0 \pmod{3}$ : $(\beta + 1)^0 + \beta^0 = 0 \neq 1$
		\item $s \equiv 1 \pmod{3}$ : $(\beta + 1)^1 + \beta^1 = 1$
		\item $s \equiv 2 \pmod{3}$ : $(\beta + 1)^2 + \beta^2 = \beta^2 + 1 + \beta^2 = 1$
	\end{enumerate}
	Thus the proof is now complete. \qed
	\item Deduce that there exists no APN power permutation on $\mathbb{F}_{2^n}$ when $n$ is even.\\
	We split the proof of this statement into two parts
	\begin{enumerate}
		\item If $s \equiv 0 \pmod{3}$ then
		\begin{align*}
			\gcd(s, 2^n - 1) &\geq \gcd(3, 2^n - 1)\\
			&\geq \gcd(2^2 - 1, 2^n - 1)\\
			&\geq 2^{\gcd(2, n)} - 1\\
			&\geq 2^{2} - 1 \quad \text{Since $n$ is even.}\\ 
			&\geq 3
		\end{align*}
		Thus $x^s$ is not a permutation when $s \equiv 0 \pmod{3}$.
		\item If $s \not\equiv 0 \pmod{3}$ then it is trivial to show that all elements of $\mathbb{F}_4$ are solutions to $(x + 1)^s + x^s = 1$, implying that the differential uniformity of $x^s$ is greater of equal to $4$. Thus $x^s$ is not an APN function.
	\end{enumerate}
	Therefore there exists no APN power permutation on $\mathbb{F}_{2^n}$ when $n$ is even.\qed
\end{enumerate}
\end{example}

\begin{theorem}
If $S$ is an $n$-bit permutation S-Box, then $\deg(S) \leq n-1$.
\end{theorem}

\begin{question} Does there exist an APN permutation on $\mathbb{F}_{2^n}$ for $n$ even ? For quite some time researchers believe that there exists no APN permutation for $n$ even, until John Dillon disprove this conjecture by giving a counter example of $6 \times 6$ APN permutation below
\begin{align*}
&0,54,48,13,15, 18, 53, 35, 25, 63, 45, 52, 3, 20, 41, 33, 59, 36, 2, 34, 10, 8, 57, 37, 60, 19, 42, 14, 50, 26, 58, 24,\\
&39, 27, 21, 17, 16, 29, 1, 62, 47, 40, 51, 56, 7, 43, 44, 38, 31, 11, 4, 28, 61, 46, 5, 49, 9, 6, 23, 32, 30, 12, 55, 22
\end{align*}
It is CCZ-equivalent to a quadratic S-Box, but note that CCZ-equivalence does not preserve the degree.\\
The big question still open whether there exists an APN function for $n \geq 8$.
\end{question}

\begin{theorem}
	An $n \times m$ function is balanced if and only if for every nonzero $\overline{b} \in \mathbb{F}_2^m$ the component function $\overline{b} \cdot S(\overline{x})$ is balanced.
\end{theorem}
\begin{proof}
	Will be completed later.\qed
\end{proof}

\begin{definition}[Nonlinearity of S-Boxes]
The nonlinearity of an S-Box $S$ is defined as the minimum nonlinearity of its component functions
$$
	\mathcal{NL}_{S} = \min \{ \mathcal{NL}_{\overline{b} \cdot S} \mid \overline{b} \in \mathbb{F}_2^m \setminus \{ \overline{0}\}\}
$$
\end{definition}

\begin{definition}[Differential Uniformity]
An $n \times m$ S-Box $S$ is said to be differentially $\delta$-uniform if for every nonzero $\overline{\alpha} \in \mathbb{F}_2^n$ and every $\overline{\beta} \in \mathbb{F}_2^m$, the equation $S(\overline{x}) \oplus S(\overline{x} \oplus \overline{\alpha}) = \beta$ has at most $\delta$ solutions.
\end{definition}

\begin{remark}[Computing differential uniformity using Gr\"{o}bner bases]
Let $f_1, f_2, \ldots, f_m$ be the coordinate functions of an $n \times m$ S-Box $S$.  For every $\overline{\alpha} \in \mathbb{F}_2^n$ and every $\overline{\beta} \in \mathbb{F}_2^m$, computing differential uniformity of $S$ is equivalent to counting the number of solutions of the following system of equations
\begin{center}
	$f_1(\overline{x}) + f_1(\overline{x} \oplus \overline{\alpha}) + \beta_1 = 0$\\
	$\vdots$\\
	$f_m(\overline{x}) + f_m(\overline{x} \oplus \overline{\alpha}) + \beta_m = 0$\\
\end{center}
Computing Gr\"{o}bner basis of the above equations can reveal the number of solutions for a given $\overline{\alpha}$ and $\overline{\beta}$.

\end{remark}

\begin{definition}[Linear Approximation Table (LAT)]
Linear approximation table (LAT) of the S-Box $S$ is defined as
$$
	\text{LAT}(\overline{\omega}, \overline{b}) = |\{ \overline{x} \in \mathbb{F}_2^n \mid \overline{\omega} \cdot \overline{x} = \overline{b} \cdot S(\overline{x})\}|- 2^{n-1}
$$
\end{definition}

\begin{definition}[Difference Distribution Table (DDT)]
Difference distribution table (DDT) of the S-Box $S$ is defined as
$$
	\text{DDT}(\overline{\alpha}, \overline{\beta}) = |\{ \overline{x} \in \mathbb{F}_2^n \mid S(\overline{x}) \oplus S(\overline{x} \oplus \overline{\alpha}) = \overline{\beta}\}|
$$
\end{definition}


\begin{definition}[Differential Branch Number]
The differential branch number of an $n \times m$ S-Box $S$ is given by
$$
	\mathcal{B}_d(S) = \min_{\overline{v}, \overline{w} \neq \overline{v}} \{ \text{wt}(\overline{v} \oplus \overline{w}) + \text{wt}(S(\overline{v}) \oplus S(\overline{w}))\}
$$
\end{definition}

\begin{definition}[Linear Branch Number]
The linear branch number of an $n \times m$ S-Box $S$ is given by
$$
	\mathcal{B}_l(S) = \min_{\overline{\alpha}, \overline{\beta}, \text{LAT}(\overline{\alpha}, \overline{\beta}) \neq 0} \{ \text{wt}(\overline{\alpha}) + \text{wt}(\overline{\beta})\}
$$
\end{definition}

\begin{definition}[Autocorrelation Table]
Autocorrelation table of the $n \times m$ S-Box $S$ is defined as follows
$$
	\text{ACT}(\overline{\alpha}, \overline{b}) = r_{\overline{b} \cdot S}(\overline{\alpha})
$$
\end{definition}

\begin{definition}[Almost Perfect Nonlinear (APN) functions]
An $n \times n$ S-Box $S$ is called almost perfect nonlinear (APN) if, for every $\overline{\alpha} \in \mathbb{F}_2^n \setminus \{ \overline{0} \}$ and every $\overline{\beta} \in \mathbb{F}_2^n$, the equation $S(\overline{x}) \oplus S(\overline{x} \oplus \overline{\alpha}) = \beta$ has either $0$ or $2$ solutions. Equivalently, the differential uniformity of $S$ is equal to $2$.
\end{definition}

\begin{definition}[Almost Bent (AB) functions]
Let $n$ be an odd positive integer. An $n \times n$ permutation S-Box $S$ that satisfy $\mathcal{NL}(S) = 2^{n-1} - 2^{(n-1)/2}$ is called almost bent (AB).
\end{definition}

\begin{theorem}
If the S-Box $S$ is almost bent, then $S$ is also APN. The converse is not necessarily true except when degree of $S$ is equal to $2$.
\end{theorem}

\begin{remark}
We can reconstruct an S-Box from its LAT, and we can also compute its DDT from LAT. However, it is not known how to recover an S-Box or its LAT from its DDT.
\end{remark}

\begin{definition}[S-Boxes with linear structures] An $n \times m$ S-Box $S$ is said to have a linear structure if there exists a nonzero $\overline{\alpha} \in \mathbb{F}_2^n$ and a nonzero $\overline{b} \in \mathbb{F}_2^m$ such that $\overline{b} \cdot S(\overline{x}) + \overline{b} \cdot S(\overline{x} \oplus \overline{\alpha})$ takes the same value $c \in \mathbb{F}_2$ for all $\overline{x}$ (i.e. the Boolean function $\overline{b} \cdot S(\overline{x})$ has a nonzero linear structure $\overline{\alpha}$).
\end{definition}

\begin{theorem}[\cite{DBLP:conf/eurocrypt/Nyberg93}]
Let $\mathbb{F}_q$ be a finite field with $q$ elements and $F : \mathbb{F}_q \mapsto \mathbb{F}_q$ defined such that
$$
	F(x) = 	\begin{cases}
				0 & \mathrm{if\ } x = 0\\
				x^{-1} & \mathrm{otherwise}
			\end{cases}
$$
The function $F$ has differential uniformity equal to $4$ in $\mathbb{F}_q$.
\end{theorem}
\begin{proof}
	Let $\alpha, \beta \in \mathbb{F}_q$ and $\alpha \neq 0$. Assume that $x \neq 0$ and $x \neq \alpha$ so that we have the following equation
	\begin{equation}
		\label{eq:inveq1}
		F(x + \alpha) - F(x) = (x+\alpha)^{-1} - x^{-1} = \beta.
	\end{equation}
	Then (\ref{eq:inveq1}) is equivalent to
	\begin{align*}
		(x + \alpha)^{-1} - x^{-1} &= \beta\\
		x(x+\alpha) ((x + \alpha)^{-1} - x^{-1}) &= x(x+\alpha) \beta\\
		\beta x^2 + \alpha \beta x - \alpha &= 0
	\end{align*}
	which has at most two solutions in $\mathbb{F}_q$. If either $x = 0$ or $x = \alpha^{-1}$ is a solution to (\ref{eq:inveq1}), then both of them are solutions when $\beta = \alpha^{-1}$. In that case the equation above can be expressed as
	\begin{equation}
		\label{eq:inveq2}
		x^2 + \alpha x - \alpha^2 = 0		
	\end{equation}
	which may give two more solutions to (\ref{eq:inveq1}).
	Let us solve (\ref{eq:inveq2}) for the case of $q = 2^n$.
	\begin{align}
		\label{eq:inveq3}
		x^2 + \alpha x + \alpha^2 &= 0 \nonumber \\
		(x^2 + \alpha x + \alpha^2)^2 &= 0^2 \nonumber \\
		x^4 + \alpha^2 x^2 + \alpha^4 &= 0 \nonumber \\
		x^4 + \alpha^2 (\alpha x + \alpha^2) + \alpha^4 &= 0 \quad \mathrm{Substitute\ } x^2 = \alpha x + \alpha^2 \nonumber \\
		x^4 + \alpha^3 x &= 0 \nonumber \\
		x(x^3 + \alpha^3) &= 0 \in \mathbb{F}_{2^n}[x].
	\end{align}
	If $n$ is odd, then by Theorem~\ref{thm:cubefuncperm} the function $x^3 + \alpha^3$ is a permutation. This implies that the only solution for (\ref{eq:inveq3}) are $x = 0$ or $x = \alpha$.
	If $n$ is even, then by Theorem~\ref{thm:cubefuncperm} the function $x^3 + \alpha^3$ is \textbf{not} a permutation and by Theorem~\ref{thm:powerfuncperm} $\gcd(3, 2^n - 1) \neq 1$, which implies that $\gcd(3, 2^n - 1) = 3$. Let $d = \dfrac{2^n - 1}{3}$. Then there are two more solutions for $x(x^3 + \alpha^3) = 0$ which are $x = \alpha^{1 + d}$ and $x = \alpha^{1 + 2d}$.\qed
\end{proof}

\bibliographystyle{plain}
\bibliography{../bibliography}

\end{document}
