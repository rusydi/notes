% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%

\makeatletter
\let\@twosidetrue\@twosidefalse
\let\@mparswitchtrue\@mparswitchfalse
\makeatother

\documentclass{../llncs}
%
\usepackage{makeidx}  % allows for indexgeneration
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[left=3cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}

\title{Finite Fields}
\author{Rusydi H. Makarim}
\institute{}
%
\begin{document}
\maketitle

\section{Preliminaries}
\begin{definition}[Equivalence relation]
	An equivalence relation $\sim$ on a set $S$ is one that satisfies these three properties for all $x, y, z \in S$
	\begin{enumerate}
		\item (Reflexive) $x \sim x$.
		\item (Symmetric) If $x \sim y$ then $y \sim x$.
		\item (Transitive) If $x \sim y$ and $y \sim z$ then $x \sim z$.
	\end{enumerate}
\end{definition}

\begin{theorem}
\label{thm:partition}
	Let $S$ be a nonempty set and let $\sim$ be an equivalence relation on $S$. Then $\sim$ yields a partition of $S$, where
	$$
		\bar{a} = \{ x \in S \mid x \sim a\}.
	$$
	Also each partition of $S$ gives rise to an equivalence relation $\sim$ on $S$ where $a \sim b$ if and only if $a$ and $b$ are in the same cell of the partition.
\end{theorem}
\begin{proof}
See page $7$ of \cite{Fraleigh02}.\qed
\end{proof}

\section{Algebraic Foundations}

\subsection{Groups}

\begin{definition}[Binary operation]
	Let $S$ be a set. A \underline{binary operation} $*$ on $S$ is a function
	\begin{align*}
		* :&\ S \times S \mapsto S\\
		   &\ (x, y) \mapsto x * y
	\end{align*}
that is, for all $x, y \in S$, there is a uniquely determined element of $S$ denoted by $x * y$ (closure property).
\end{definition}

\begin{definition}[Group]
	Let $G$ be a set together with a binary operation $*$ satisfy the following property
	\begin{enumerate}
		\item The binary operation $*$ is associative, that is for all $x, y \in G$ then $(x * y) * z = x * (y * z)$.
		\item There exists an element $e \in G$ such that for all $x \in G$, then $x * e = e * x = x$. The element $e$ is called the identity element of $G$ for the operation $*$.
		\item For all $x \in G$, there exists $x' \in G$ such that $x * x' = x' * x = e$. The element $x'$ is called the inverse of $x$ for the operation $*$.
	\end{enumerate}
	Then $G$ is called a \underline{group} under operation $*$. Furthermore, if $*$ is commutative, that is $x * y = y * x$ for all $x, y \in G$ then $G$ is called \underline{commutative/abelian group}.
\end{definition}

\begin{remark}
	$\mathbb{N} \subset \mathbb{Z} \subset \mathbb{Q} \subset \mathbb{R} \subset \mathbb{C}$
	\begin{enumerate}
		\item $\mathbb{N}$ is not a group under either addition or multiplication. There exist some elements of $\mathbb{N}$ that do not have inverse.
		\item The set of rational number $\mathbb{Q}$ is a group under addition $+$. However, $\mathbb{Q}$ is not a group under multiplication since the element $0 \in \mathbb{Q}$ does not have an inverse. If we exclude $0$, then the set $\mathbb{Q}^* = \mathbb{Q} \setminus \{ 0 \}$ is a group under multiplication.
		\item The set of real numbers $\mathbb{R}$ and complex numbers $\mathbb{C}$ are both groups under addition and multiplication.
	\end{enumerate}
\end{remark}

\begin{theorem}
The identity element and inverse of each element are unique in a group.
\end{theorem}
\begin{proof}
Let $G$ be a group. We will first prove the uniqueness of the identity element.
Let $e$ and $e'$ be two identity elements of $G$. Regarding $e$ as an identity element, we have $e * e' = e'$. Similarly, treating $e'$ as an identity element yields $e * e' = e$. This implies that $e = e'$, proving that an identity element must be unique.\\
In order to show the uniqueness of an inverse, let $a \in G$ and suppose that it has inverses $a'$ and $a''$ so that $a' * a = a * a' = e$ and $a * a'' = a'' * a = e$. It follows that
$$
	a * a' = a * a'' = e
$$
and using cancellation law, we obtain $a' = a''$. So the inverse of $a$ is unique in a group.\qed
\end{proof}

\begin{remark}
	Let $G$ be a group, $a \in G$ and $n \in \mathbb{Z}$.
	\begin{enumerate}
		\item Multiplication notation.
		$$
			a^n = 
			\begin{cases}
				\underbrace{a \cdot a \cdots a}_{n \text{ times}} & \text{if } n > 0\\
				e & \text{if } n = 0\\
				\underbrace{a^{-1} \cdot a^{-1} \cdots a^{-1}}_{-n \text{ times}} & \text{if } n < 0
			\end{cases}
		$$
		\item Additive notation.
		$$
			na = 
			\begin{cases}
				\underbrace{a + a + \ldots + a}_{n \text{ times}} & \text{if } n > 0\\
				e & \text{if } n = 0\\
				\underbrace{(-a) + (-a) + \ldots + (-a)}_{-n \text{ times}} & \text{if } n < 0
			\end{cases}			
		$$
	\end{enumerate}
\end{remark}

\begin{remark}[Example of Groups]
	\begin{enumerate}
		\item The subset $\{ \pm 1 \} \subset \mathbb{Z}$ is a group under multiplication.
		\item The subset $\{ e \}$ is the trivial group.
		\item The set $\mathbb{Z}/n\mathbb{Z}$ is a group under addition for any $n \in \mathbb{Z}$.
	\end{enumerate}
\end{remark}

\begin{definition}[Subgroup]
A subset $H$ of $G$ is called a subgroup of $G$ if $H$ itself a group under the same binary operation as in $G$, denoted $H \leq G$.
\end{definition}

\begin{proposition}
\label{prop:subgroup_characterization_1}
Let $G$ be a group and $H \subseteq G$. Then $H$ is a subgroup of $G$ if and only if
\begin{enumerate}
	\item for all $x, y \in H$ we have $x * y \in H$ (closure property).
	\item The identity $e$ is in $H$.
	\item For all $x \in H$ then $x^{-1} \in H$.
\end{enumerate}
\end{proposition}

\begin{proposition}
\label{prop:subgroup_characterization_2}
Let $G$ be a group and $H \subseteq G$. Then $H$ itself is a subgroup of $G$ if and only if
\begin{enumerate}
	\item $H$ is nonempty.
	\item For all $x, y \in H$ then $x * y^{-1} \in H$.
\end{enumerate}
\end{proposition}

\begin{proposition}
Let $H$ be a subgroup of $G$. For all $a, b \in G$ the relation
$$
	a \equiv b \pmod{H} \Leftrightarrow b^{-1}a \in H
$$
is an equivalence relation on $G$ called the congruence $\pmod{H}$ on $G$. The sets
$$
	bH = \{ bh \mid h \in H\}
$$
for $b \in G$ are the equivalence classes, called the left cosets of $G \bmod{H}$. 
\end{proposition}

\begin{remark}
Similarly, the sets
$$
	Hb = \{ hb \mid h \in H\}
$$
are called the right cosets of $G$ modulo $H$. They are the equivalence classes of the following relation
$$
	a \equiv b \bmod{H} \Leftrightarrow ab^{-1} \in H.
$$
\end{remark}

\begin{remark}
	If $G$ is an Abelian group, then $Ha = aH$ for all $a \in G$, i.e. the left and right cosets coincides.
\end{remark}

\begin{definition}
Let $G$ be a group. If $G$ has finitely many distinct left (or right) cosets modulo $H$, then their number is called the index of $H$ in $G$. We denote it by $(G : H)$.
\end{definition}

\begin{definition}[Cyclic Groups]
	Let $G$ be a group and $a \in G$, the set
	$$
		\langle a \rangle = \{ a^{n} \mid n \in \mathbb{Z} \}
	$$
	is a subgroup called the cyclic subgroup of $G$ generated by $a \in G$.
\end{definition}

\begin{theorem}
\label{thm:lagrange}
Let $G$ be a finite group and $H \leq G$. Then
$$
	|G| = |H| \cdot (G : H).
$$
In particular $|H|$ divides $|G|$ and $\text{ord}(a)$ divides $|G|$ for any $a \in G$.
\end{theorem}
\begin{proof}
By Theorem~\ref{thm:partition} the distinct cosets $bH$ form a partition of $G$. So $|G|$ is equal to the sum of cardinalities of the distinct cosets $bH$. But $|bH| = |H|$ so
\begin{align*}
	|G| &= |H| \cdot (\text{the number of distinct cosets})\\
	&= |H| \cdot (G : H).
\end{align*}
Clearly $|H|$ divides $|G|$. In particular $a \in G$ and $\text{ord}(a) = |\langle a \rangle|$ where $\langle a \rangle$ is the cyclic group generated by $a$. Applying general results with $H = \langle a \rangle$, implies that $\text{ord}(a)$ divides $|G|$.\qed
\end{proof}

\begin{remark}
	If $G$ is finite, then by Theorem~\ref{thm:lagrange}, the number of left cosets is equal to the number of right cosets. We call this the \underline{index of H in G} and we denote by $(G : H)$
\end{remark}

\begin{theorem}
We have the following results on cyclic groups.
\begin{enumerate}
	\item Every subgroup of a cyclic group is cyclic.
	\item In a cyclic group of order $m$, the element $a^k$ generates a subgroup of order $m/\gcd(m, k)$.
	\item If $d \mid m$, $d > 0$ where $m = |\langle a \rangle|$ then $\langle a \rangle$ contains one and only one subgroup of index $d$. Clearly for any positive divisor $f \mid m$, $\langle a \rangle$ contains one and only one subgroup of order $f$.
	\item Let $f \mid m$, $f > 0$ where $m = \text{ord}(a)$. Then $\langle a \rangle$ contains $\varphi(f)$ element of order $f$ ($\varphi$ is the euler's phi function).
	\item A finite cyclic group $\langle a \rangle$ of order $m$ contains $\varphi(m)$ generators, i.e. $\langle a \rangle = \langle a^r \rangle$ $1 \leq r \leq m$ with $\gcd(r, m) = 1$.
\end{enumerate}
\end{theorem}
\begin{proof}
See page $7$ of \cite{LNiederreiter09}. \qed
\end{proof}

\begin{definition}[Group Homomorphism]
	Let $G$ and $G'$ be two groups with respective binary operations $*$ and $\bullet$. A map $f : G \mapsto G'$ is a group homomorphism if
	$$
		f(x * y) = f(x) \bullet f(y)
	$$
	for any $x, y \in G$.
\end{definition}

\begin{remark}
\label{rmrk:id_and_inv_hom}
	Let $G, G'$ be groups with respective identity $e, e'$. Let $f : G \mapsto G'$ be a homomorphism. Then $f(e) = e'$ and $f(x^{-1}) = f(x)^{-1}$ for any $x \in G$. This can be shown as follows :
	$$
		f(e) = f(e * e) = f(e) \bullet f(e)
	$$
	and by the cancellation law, clearly $f(e) = e'$. Similarly
	$$
		e' = f(e) = f(x * x^{-1}) = f(x) \bullet f(x^{-1})
	$$
	and thus $f(x^{-1}) = f(x)^{-1}$.
\end{remark}

\begin{proposition}
	Let $f : G \mapsto G'$ be a homomorphism of groups $G$ and $G'$. Then
	\begin{enumerate}
		\item $\text{Im}_f = f(G) = \{ f(g) \mid g \in G \}$ is a subgroup of $G'$ called the image of $f$.
		\item $\text{Ker}_f = \{ x \in G \mid f(x) = e'\}$ is a subgroup of $G$, called the kernel of $f$.
	\end{enumerate}
\end{proposition}
\begin{proof}
For part(1), let $f(g_1), f(g_2) \in \text{Im}_f$ for some $g_1, g_2 \in G$. Then
\begin{align*}
	f(g_1) \bullet f(g_2) &= f(g_1 * g_2) \quad \text{Using homomorphicity of $f$}\\
	&= f(g_1 * g_2)\\
	&= f(g_3) \qquad \quad \text{$G$ is a group, multiplication is closed, hence $g_3 \in G$}
\end{align*}
By definition of $\text{Im}_f$, clearly multiplication in $\text{Im}_f$ is closed. For the identitiy $e \in G$, the element $f(e)$ acts as the identity in $\text{Im}_f$. For $g \in G$, we have $f(g^{-1}) \in \text{Im}_f$ and by the homomorphicity of $f$ it implies that $f(g) \bullet f(g^{-1}) = f(g) \bullet f(g)^{-1} = e'$, thus $f(g^{-1})$ is the inverse of $f(g)$ in the image of $f$.\\
For part (2), let $x, y \in \text{Ker}_f$. Then $f(x * y) = f(x) \bullet f(y) = e' \bullet e' = e'$, and thus $x*y \in \text{Ker}_f$. The identity $e$ is in $\text{Ker}_f$ by Remark~\ref{rmrk:id_and_inv_hom}. For $x \in \text{Ker}_f$, we have $e' = f(e) = f(x * x^{-1}) = f(x) \bullet f(x^{-1}) = f(x^{-1})$ thus $x^{-1} \in \text{Ker}_f$.\qed
\end{proof}


\subsection{Rings and Fields}

\begin{theorem}[Newton's formula] Let $\sigma_1, \ldots, \sigma_n$ be the elementary symmetric polynomials in $x_1, \ldots, x_n$ over $R$, and let $s_0 = n \in \mathbb{Z}$ and $s_k = s_{k}(x_1, \ldots, x_n) = x_1^k + \ldots + x_n^k \in R[x_1, \ldots, x_n]$ for $k \geq 1$. Then the formula
$$
	s_k - s_{k-1} \sigma_1 + s_{k-2} \sigma_2 + \ldots + (-1)^{m-1} s_{k-m+1} \sigma_{m-1} + (-1)^m \frac{m}{n} s_{k-m} \sigma_m = 0
$$
holds for $k \geq 1$, where $m = \min(k, n)$.
\end{theorem}


\subsection{Univariate Polynomials}

\subsection{Field Extensions}


\section{Structure of Finite Fields}

\newpage

\section{Exercises}
\begin{enumerate}
	\item  Let $G = \left\lbrace \begin{pmatrix} a & b\\c & d\end{pmatrix} \mid ad - bc \neq 0 \text{ and } a,b,c,d \in\mathbb{R} \right\rbrace$. Show that $G$ is a non-commutative group under the multiplication of matrices.\\
	\textbf{Answer} : 
	\begin{itemize}
		\item (Non-Commutativity) Let $M, N \in G$ where $M = \begin{pmatrix} a & b\\c & d\end{pmatrix}$ and $N = \begin{pmatrix} e & f\\g&h \end{pmatrix}$. We have
		\begin{align*}
			MN &= \begin{pmatrix} a & b\\c & d\end{pmatrix} \begin{pmatrix} e & f\\g&h \end{pmatrix} = \begin{pmatrix} ae + bg & af + bh \\ ce + dg & cf + dh \end{pmatrix}\\
			NM &= \begin{pmatrix} e & f\\g&h \end{pmatrix} \begin{pmatrix} a & b\\c & d\end{pmatrix} = \begin{pmatrix} ae + cf & be + df \\ ag + ch & bg + dh \end{pmatrix}
		\end{align*}
		Since $MN \neq NM$ then the operation defined in $G$ is not commutative
	
		\item (Associativity) Let $ M_1 = \begin{pmatrix} a & b\\c & d \end{pmatrix}$, $M_2 = \begin{pmatrix} e & f\\g & h \end{pmatrix}$, $M_3 = \begin{pmatrix} i & j\\k & l \end{pmatrix}$ and $M_1, M_2, M_3 \in G$. We need to show that $[M_1 M_2] M_3 = M_1 [M_2 M_3]$
		\begin{align*}
			[M_1 M_2] M_3 &= \left[ \begin{pmatrix} a & b\\c & d \end{pmatrix} \begin{pmatrix} e & f\\g & h \end{pmatrix} \right] \begin{pmatrix} i & j\\k & l \end{pmatrix} = \begin{pmatrix} ae + bg & af + bh\\ce+dg & cf + dh\end{pmatrix} \begin{pmatrix} i & j\\k & l \end{pmatrix}\\
			&= \begin{pmatrix} aei+bgi+afk+bhk & aej + bgj +afl + bhl \\ cei+dgi+cfk+dhk & cej + dgj + cfl + dhl\end{pmatrix}\\
			M_1 [M_2 M_3] &=  \begin{pmatrix} a & b\\c & d \end{pmatrix} \left[ \begin{pmatrix} e & f\\g & h \end{pmatrix}  \begin{pmatrix} i & j\\k & l \end{pmatrix} \right] = \begin{pmatrix} a & b\\c & d \end{pmatrix} \begin{pmatrix} ei+fk & ej+fl \\ gi+hk & gj+hl\end{pmatrix}\\
			&= \begin{pmatrix} aei + afk + bgi + bhk & aej + afl + bgj + bhl \\ cei + cfk + dgi + dhk & cej + cfl + dgj + dhl\end{pmatrix}
		\end{align*}
		Since $[M_1 M_2] M_3 = M_1 [M_2 M_3]$ then the matrix multiplication operation on the set $G$ satisfy the associativity property.
		
		\item (Existence of Identity) Let $I = \begin{pmatrix}	1 & 0 \\ 0 & 1 \end{pmatrix} \in G$ and let $M = \begin{pmatrix} a & b \\ c & d \end{pmatrix} \in G$. Then
		\begin{align*}
			MI = \begin{pmatrix} a & b \\ c & d \end{pmatrix} \begin{pmatrix}	1 & 0 \\ 0 & 1 \end{pmatrix} = \begin{pmatrix}	1 & 0 \\ 0 & 1 \end{pmatrix} \begin{pmatrix} a & b \\ c & d \end{pmatrix} = IM = M
		\end{align*}
		Therefore $I$ is the identity element and the operation above is true for any $M \in G$.
		
		\item (Existence of Inverse Element) Let $M = \begin{pmatrix} a & b \\ c & d \end{pmatrix} \in G$. The inverse of $M$, denoted $M^{-1}$, is exist in $G$ if and only if $M M^{-1} = M^{-1} M = I$ where $I$ is the identity element in $G$. Let $M^{-1} = \begin{pmatrix} \frac{d}{ad-bc} & \frac{-b}{ab-bc} \\ \frac{-c}{ad-bc} & \frac{a}{ad-bc} \end{pmatrix}$. Then
		$$
			M M^{-1} = \begin{pmatrix} a & b \\ c & d \end{pmatrix} \begin{pmatrix} \frac{d}{ad-bc} & \frac{-b}{ab-bc} \\ \frac{-c}{ad-bc} & \frac{a}{ad-bc} \end{pmatrix} = \begin{pmatrix} 1 & 0\\0 & 1 \end{pmatrix} = M^{-1} M = I
		$$
		
		Since $ad - bc \neq 0$ and $a, b, c, d \in \mathbb{R}$ then for every $M \in G$ the inverse $M^{-1}$ is also in $G$.
	\end{itemize}			
	
	Because the set $G$ under matrix multiplication satisfies all the above properties, therefore the set $G$ is a non-commutative group under matrix multiplication.
	\item Prove that Proposition~\ref{prop:subgroup_characterization_1} is equivalent to Proposition~\ref{prop:subgroup_characterization_2}.
	
	\item Let $H$ be a subgroup of $G$. Proof that, for $a, b \in G$ the relation $a \equiv b \pmod{H}$ holds if and only if $b^{-1}a \in H$, is an equivalence relation on $G$ called the congruence modulo $H$ on $G$. Also show that the set $bH = \{ bh | h \in H \}$ are the equivalence classes (left cosets of $G$ modulo $H$).
	
	\item Let $G$ be a group and $H \leq G$. For any $a \in G$, proof that the cardinality $aH$ is equal  to $H$. Similarly for any $a \in G$, proof that the cardinality of $Ha$ is equal to $H$.
	
	\item Let $G$ be a group. Using the multiplicative notation, proof that the set $\langle a \rangle = \{ a^n | n \in \mathbb{Z} \}$ is a subgroup of $G$ .
	
	\item Let $G = \mathbb{Z}$ and $G' = \mathbb{Z} / n \mathbb{Z}$. Let $f : \mathbb{Z} \mapsto \mathbb{Z} / n \mathbb{Z}$ that is $f : x \mapsto \bar{x} = x + n\mathbb{Z}$
	\begin{enumerate}
		\item Show that the identity of $\mathbb{Z}/n \mathbb{Z}$ is $\bar{0} = n\mathbb{Z}$
		\item Show that $f$ is a homomorphism
		\item Find $Im(f)$
		\item Find $Ker_f = \{ x \in \mathbb{Z} | f(x) = \bar{0}\}$
	\end{enumerate}	
	
	\item (Homomorphism Theorem). Let $G$ and $G'$ be groups and $f : G \mapsto G'$ be a homomorphism. Verify that $\bar{f} : G/Ker_f \mapsto Im(f)$ is an isomorphism where $Ker_f = \{ x \in G | f(x) = e'\}$ and $Im(f) = \{ f(g) | g \in G\}$
	
	\item Let $G$ be a group. We define $C$, the center of $G$, as follows
	$$
		C = \{ c \in G | cg = gc \quad \forall g \in G\}
	$$
	Proof that $C$ is a normal subgroup of $G$ which is commutative.	
	
	\item Express $s_5(x_1, x_2, x_3, x_4) = x_1^5 + x_2^5 + x_3^x5 + x_4^5 \in \mathbb{F}_3[x_1, x_2, x_3, x_4]$ in terms of the elementary symmetric polynomials $\sigma_1, \sigma_2, \sigma_3, \sigma_4$.\\
	\textbf{Answer} : Recall that the Newton's law gives an expression of $s_k$ as
	$$
		s_k = s_{k-1} \sigma_1 - s_{k-2} \sigma_2 - \cdots - (-1)^{m-1} s_{k-m+1} \sigma_{m-1} - (-1)^m \frac{m}{n} s_{k-m} \sigma_m.
	$$
	where $m = \min(n, k)$. In this case, $k = 5$ and $n = 4$, hence $m = 4$. The polynomial $s_5$ can then be expressed as
	$$
		s_5 = s_4 \sigma_1 - s_3 \sigma_2 + s_2 \sigma_3 - s_1 \sigma_4.
	$$
	\item If $\theta$ is algebraic over $L$ and $L$ is an algebraic extension of $K$, then $\theta$ is algebraic over $K$. Thus show that if $F$ is an algebraic extension of $L$, then $F$ is an algebraic extension of $K$.\\
	\textbf{Answer} : If $\theta$ is algebraic over $L$, then there exists a nonzero polynomial $f(x) = a_n x^n + a_{n-1} x^{n-1} + \cdots + a_1 x + a_0 \in L[x]$ with $a_i \in L$ such that $f(\theta) = 0$. Since $L$ is an algebraic extension of $K$, then every element of $L$ is algebraic over $K$. This implies that for all $a_i$, there exists a nonzero polynomial $g_i \in K[x]$ such that $g_i(a_i) = 0$.\\
	Equivalently, $a_i$ is a root of $g_i$. Then $g_i$ can be written as $g_i(x) = g'_i(x) (x - a_i)$ with $\deg(g_i') < \deg(g_i)$ and hence $a_i$ can be expressed as
	$$
		a_i = x - \dfrac{g_i}{g'_i}.
	$$
It follows that the polynomial $f$ can be written as
$$
	f(x) = \left(x - \dfrac{g_n}{g'_n}\right) x^n + \left( x - \dfrac{g_{n-1}}{g'_{n-1}} \right) x^{n-1} + \cdots + \left( x - \dfrac{g_1}{g'_1}\right) x + \left( x - \dfrac{g_0}{g'_0}\right).
$$
Because the coefficients of $g_i$ and $g'_i$ lies in $K$, thus the polynomial $f$ can be seen as an element of $K[x]$. Therefore $\theta$ is algebraic over $K$.\\
So if $F$ is algebraic extension of $L$, every element of $F$ is algebraic over $K$. It immediately follows that $F$ is an algebraic extension over $K$.
	\item Construct $\mathbb{F}_{16}$ as a finite extension of $\mathbb{F}_4$. \textbf{Hint} : You must first explicitly construct $\mathbb{F}_4$ as an extension of $\mathbb{F}_2$. Then find a monic irreducible polynomial $f \in \mathbb{F}_4[x]$ of degree $2$ and construct $\mathbb{F}_{16} \simeq \mathbb{F}_4[x]/ \langle f \rangle$.
	\item Prove that for $a \in \mathbb{F}_{2^n}^*$ and $b \in \mathbb{F}_{2^n}$, the equation $x^2 + ax + b \in \mathbb{F}_{2^n}$ has no solution if and only if $\mathrm{Tr}(b/a^2) \neq 0$.\\
	\textbf{Answer} : ($\Rightarrow$) We will use proof by contrapositive. Let $\mathrm{Tr}(b/a^2) = 0$. We can rewrite the equation $x^2 + ax + b = 0$ by multiplying both sides by $1/a^2$ giving $(x/a)^2 + (x/a) + (b/a^2) = 0$. By computing the absolute trace of the new equation, we have
	\begin{align*}
		\mathrm{Tr}((x/a)^2 + (x/a) + (b/a^2)) &= 0\\
		\mathrm{Tr}((x/a)^2) + \mathrm{Tr}(x/a) + \mathrm{Tr}(b/a^2) &= 0\\
		\mathrm{Tr}((x/a)^2) + \mathrm{Tr}(x/a) &= 0
	\end{align*}
	Hence $x/a = 1$ satisfy the above equation, which implies that $x = a^{-1}$ is a solution to the equation $x^2 + ax + b = 0$.\\
	($\Leftarrow$) Again, we will also use a proof by contraposition. Let $x^2 + ax + b = 0$ has a solution, say $r \in \mathbb{F}_{2^n}$. With similar approach by computing the trace of $(r/a)^2 + (r/a) + b/a^2$, we have
	\begin{align*}
		\mathrm{Tr}(b/a^2) &= \mathrm{Tr}(r^2 / a^2) + \mathrm{Tr}(r/a)\\
		&= \sum_{i=1}^{n-1} (r^2/a^2)^{2^i} + \sum_{j=1}^{n-1} (r/a)^{2^j}\\
		&= 0
	\end{align*}
	because $r^{2^n} / a^{2^n} = r / a$. Thus the proof is complete.\qed
	
	\item Let $\mathbb{F}_q$ be a finite field of order $q = p^n$ for some prime $p$ and positive integer $n$.
	\begin{enumerate}
		\item Prove that $\mathbb{F}_q^n$ is a ring under component-wise addition and multiplication.
		\item Prove that the mapping $\phi : \mathbb{F}_q \mapsto \mathbb{F}_q^n$ defined by
		$$
			\phi(a) = (a^{q^0}, a^{q^1}, \ldots, a^{q^{n-1}})
		$$
		is an injective ring homomorphism.
	\end{enumerate}
\end{enumerate}

\bibliographystyle{plain}
\bibliography{../bibliography}

\end{document}
